// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify  from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'vuetify/src/stylus/main.styl' // Ensure you are using stylus-loader

import App from './App' 
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
//import App from './App' 
import test from './components/Test'
import loginpage from './components/loginpage'
import selectcmpnt from './components/selectcmpnt'
import formcmpnt from './components/formcmpnt'
import menucmpnt from './components/menucmpnt'
import textfieldcmpnt from './components/textfieldcmpnt'

Vue.use(Vuetify)
Vue.use(VueResource)
Vue.use(VueRouter)

Vue.config.productionTip = false

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/loginpage', component: loginpage },
    { path: '/selectcmpnt', component: selectcmpnt },
    { path: '/textfieldcmpnt', component: textfieldcmpnt },
    { path: '/formcmpnt', component: formcmpnt },
    { path: '/menucmpnt', component: menucmpnt },
    { path: '/test', component: test } ]
})

/* eslint-disable no-new */
new Vue({
  router,
  template: `
    <div id="app">
      <ul>
        <li  style="display: inline;"><router-link to="/loginpage">loginpage</router-link></li>
        <li   style="display: inline;"><router-link to="/test">test</router-link></li>
        <li style="display: inline;"><router-link to="/menucmpnt">menutcmpnt</router-link></li>
        <li style="display: inline;"><router-link to="/formcmpnt">formcmpnt</router-link></li>
        <li style="display: inline;"><router-link to="/textfieldcmpnt">textfieldcmpnt</router-link></li>
        <li style="display: inline;"><router-link to="/selectcmpnt">selectcmpnt</router-link></li>
      </ul>
      <router-view></router-view>
    </div>
    `
}).$mount('#app')
